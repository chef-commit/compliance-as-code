pkg_name=audit-commit-baseline
pkg_version=0.1.0
pkg_origin=gsloan-chef
pkg_maintainer="The Habitat Maintainers <humans@habitat.sh>"
pkg_license=("Apache-2.0")
pkg_description="Effortless Linux Audit Example"
pkg_scaffolding="chef/scaffolding-chef-inspec"
scaffold_chef_license="accept-no-persist"
